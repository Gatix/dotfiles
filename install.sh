ln -sf  ~/.config/vimrc ~/.vimrc
ln -sf  ~/.config/zshrc ~/.zshrc
ln -sf  ~/.config/Xdefaults ~/.Xdefaults
ln -sf  ~/.config/zprofile ~/.zprofile
ln -sf  ~/.config/gitconfig ~/.gitconfig
ln -sf  ~/.config/mpdconf ~/.mpdconf
# ln -sf  ~/.config/compton.conf ~/.compton.conf
# ln -sf  ~/.config/fehbg ~/.fehbg
ln -sf  ~/.config/xinitrc ~/.xinitrc
ln -sf  ~/.config/tmux.conf ~/.tmux.conf
ln -sf  ~/.config/gemrc ~/.gemrc
