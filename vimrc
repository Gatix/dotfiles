set nocompatible

set rtp+=~/.vim/bundle/vundle/ 
call vundle#rc() 

Bundle 'gmarik/vundle'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-fugitive'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/nerdcommenter'
Bundle 'Lokaltog/vim-powerline'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'kien/ctrlp.vim'
Bundle 'mbbill/undotree'
Bundle 'spf13/PIV'

filetype on
syntax on

set t_Co=256
set encoding=utf-8
set laststatus=2

set number
set scrolloff=5
"set title
set nowrap

set history=200

" Files
set autochdir
set autoread
set confirm
"set backup
"set backupdir=~/.vim/backup,/tmp
"set backupext=~

"set autoindent
set backspace=indent,eol,start

" Binds
cmap w!! w !sudo tee % >/dev/null
nnoremap <CR> :noh<CR><CR>

set wrapscan
set smartcase
set ignorecase
set incsearch
set showmatch
set hlsearch

" Plugin Specifics
map <C-e> :NERDTreeToggle<CR>:NERDTreeMirror<CR>
map <leader>e :NERDTreeFind<CR>
noremap <Leader>u :UndotreeToggle<CR>
let g:undotree_SetFocusWhenToggle=1

