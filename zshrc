export EDITOR=vim
export PAGER=less
export LANG="en_US.UTF-8"
export HISTFILE=~/.histfile
export HISTSIZE=1000
export SAVEHIST=1000 export DIRSTACKSIZE=16

setopt appendhistory
setopt autocd
setopt interactive_comments
setopt correct
setopt menu_complete
unsetopt beep

zstyle :compinstall filename '/home/gatix/.zshrc'
autoload -Uz compinit
compinit

bindkey -v
bindkey -e
bindkey "\e[5~" beginning-of-history # PageUp
bindkey "\e[6~" end-of-history # PageDown
bindkey "\e[2~" quoted-insert # Ins
bindkey "\e[3~" delete-char # Del
bindkey "\e[Z" reverse-menu-complete # Shift+Tab
bindkey -M viins ' ' magic-space 
# for rxvt
bindkey "\e[7~" beginning-of-line # Home
bindkey "\e[8~" end-of-line # End

# Aliases
alias svim='sudo vim'
alias !='sudo'
alias c='clear'
alias e='exit'
alias ls='ls --color=auto' 
alias l='ls -lFh'  
alias la='ls -lAFh'   
alias lr='ls -tRFh'   
alias lt='ls -ltFh'   
alias rm='mv -t ~/.local/share/Trash/files'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p -v'
alias df='df -h'
alias du='du -h -c'
alias reload='source ~/.zshrc'
alias remdir='"rm" -rf'
alias trashman='"rm" -rf ~/.local/share/Trash/files/*'
alias zshrc="$EDITOR ~/.zshrc"
alias lesslast="less !:*"
alias mine="sudo chown ro"
alias -g G="| grep"
alias -g L="| less"
alias locate='locate -e -L'
alias be='bundle exec'
alias httpserver='python -m http.server'

PS1='%(?.%F{green}.%F{red})%#%f '
RPS1='[%B%F{blue}%~%f%b]'

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

clear
archey
